<!DOCTYPE html>
<html lang="en">
<head>
	<title>KHS Mahasiswa</title>
	<script src=”https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js”></script>
	<style type="text/css">
		body{
			font-family: sans-serif;
			background: #d5f0f3;
		}

		h1{
			text-align: center;
			/*ketebalan font*/
			font-weight: 300;
		}
		.konten{
			width: 700px;
			background: white;
			/*meletakkan form ke tengah*/
			margin: 80px auto;
			padding: 30px 20px;
		}
		.teks{
			text-align: center;
			/*membuat semua huruf menjadi kapital*/
			text-transform: uppercase;
		}
	</style>
</head>
<body>
	<h1>Sistem KHS sederhana</h1>
	<div class="konten">
		<table border="1" align="center">
			<th>DATA Mahasiswa</th>
			<th>Nilai</th>
			<th>Action</th>
			<?php foreach ($posts as $post): ?>
			<tr>
				<td>NIM</td>
				<td><p><?php echo $post->NIM; ?></p></td>
				<td></td>
			</tr>
			<tr>
				<td>Nama</td>
				<td><p><?php echo $post->NAMA; ?></p></td>
				<td> <a href="<?php echo base_url('Khs/update_process'); ?>">Edit</a> </td>
			</tr>
			<tr>
				<td>Alamat</td>
				<td><p><?php echo $post->ALAMAT; ?></p></td>
			</tr>
			<?php endforeach; ?>
		</table>
		<br><br>
		<p class="teks">Nilai KHS</p>
		<table border="1" align="center">
			<th>Pemrograman Dasar</th>
			<th>Pemrograman Lanjut</th>
			<th>Dasar Basis Data</th>
			<th>Administrasi Basis Data</th>
			<tr align="center">
			    <?php foreach ($posts as $post): ?>
				<td><p><?php echo $post->PEMDAS; ?></p></td>
				<td><p><?php echo $post->PEMLAN; ?></p></td>
				<td><p><?php echo $post->DASAR_DB; ?></p></td>
				<td><p><?php echo $post->ADMIN_DB; ?></p></td>
				<?php endforeach; ?>
			</tr>
		</table>
		<br><br>
		<table border="0" align="center">
			<tr>
				<th align="center">
					<a href="<?php echo base_url('login/logout'); ?>">Logout</a>
				</th>
			</tr>
		</table>
	</div>
</body>
</html>