<?php
    class Khs extends CI_Controller{
        public function index(){
            $this->load->helper('url');
            $this->load->model('Model');
            $posts = $this->Model->get_khs();
            $data['posts'] = $posts;
            $this->load->view('khs', $data);
        }
        
        public function update($NIM){
            $this->load->helper('url');
            $loc = array('NIM' => $NIM);
            $data['mhs'] = $this->Model->edit($loc)->result();
            $this->load->view('update_form', $data);
        }
        
        public function update_process(){
            $NIM = $this->input->post('NIM');
            $nama = $this->input->post('nama');
            $alamat = $this->input->post('alamat');
            $this->load->helper('url');
            $this->load->model('Model');
            $this->Model->update($NIM, $nama, $alamat);
            redirect(base_url('Khs/index'), 'refresh');
        }
    }
?>