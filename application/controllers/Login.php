<?php
	/**
	 * 
	 */
	class Login extends CI_Controller{
	    
	    public function __construct(){
	        parent::__construct();
	        $this->load->model('Model');
	    }
		
		public function index(){
			$this->load->view('login_form');
		}

		public function action(){
			$NIM = $this->input->post('NIM');
		    $password = $this->input->post('password');
		    $where = array(
    			'NIM' => $NIM,
	    		'password' => md5($password)
		    	);
		    $cek = $this->Model->cek_login('mhs',$where)->num_rows();
		    if($cek > 0){
			    $data_session = array(
				    'NIM' => $NIM,
    				'status' => "login"
	    			);
			    $this->session->set_userdata($data_session);
 
    			redirect(base_url('Khs/khs'));
 
	    	}else{
		    	echo "Username dan password salah !";
		    }
		}

		public function logout(){
		    $this->session->sess_destroy();
			redirect(base_url('login'));
	    }
	}
?>