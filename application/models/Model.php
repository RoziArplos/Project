<?php
	/**
	 * 
	 */
	class Model extends CI_Model{

		function cek_login($table,$where){
			$this->load->database();
			return $this->db->get_where($table,$where);
		}
		
		public function get_biodata($NIM){
			$this->load->database();
			$query = $this->db->query(
				"SELECT m.NIM, m.NAMA, m.ALAMAT, p.PRODI 
				 FROM mhs m
				 JOIN prodi p
				 	ON m.ID_PRODI = p.ID_PRODI
				 WHERE m.NIM = $NIM
				");
			return $query->result();
		}

		function edit($loc){
			$this->load->database();
			return $this->db->get_where('mhs',$loc);
		}

		function update($NIM, $nama, $alamat){
			$this->load->database();
			$this->db->query("UPDATE mhs SET NAMA = '$nama', ALAMAT = '$alamat' WHERE NIM = '$NIM'");
		}

		public function get_khs($NIM){
			$this->load->database();
			$query = $this->db->query(
				"SELECT k.PEMDAS, k.PEMLAN, k.DASAR_DB, k.ADMIN_DB 
				 FROM khs k
				 JOIN mhs m
				 	ON k.NIM = m.NIM
				 WHERE m.NIM = $NIM
				");
			return $query->result();
		}


	}
?>